terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.19.0"
    }
     kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.23.0"
    }
  }
}
#Default AWS provider
provider "aws" {
  profile = "default"
  region = var.region
  default_tags {
    tags = {
      managedBy   = "Terraform"
      env         = var.env
      uid         = var.uid
    }
  }
  ignore_tags {
    keys = ["env", "uid"]
  }
}