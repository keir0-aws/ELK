output "eks_cluster_endpoint" {
  value = module.eks.cluster_endpoint
}

output "eks_cluster_certificate_authority" {
  value = module.eks.cluster_certificate_authority_data
}

output "user_1_access_key" {
  value = module.user1_iam_user.iam_access_key_id
}

output "user_1_secret" {
  value = module.user1_iam_user.iam_access_key_secret
  sensitive = true
}

output "eks_admin_role_arn" {
  value = module.eks_admins_iam_role.iam_role_arn
}