locals {
  project = "ELK"
  uid     = var.uid == "" ? "" : ".${var.uid}"
}

variable "region" {
  type        = string
  default     = "us-east-1"
  description = "aws default region"
}

variable "env" {
  type        = string
  default     = "dev"
  description = "Environment can be prod, dev, or test"
}

variable "uid" {
  type        = string
  default     = ""
  description = "Unique id to have independant instances in the same aws account"
}